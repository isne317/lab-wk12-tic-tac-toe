class Bot
{
public:
	Bot()
	{
	
	}
	
	int think(Board *b)
	{
		srand(time(0));
		//find winning spot
		for(int i = 0; i < 9; i++)
		{
			if(b->getPos(i) >= 8)
			{
				b->place(2,i);
				return i+1;
			}
		}
		/*
			defend the point
			- reason being that bot has to prioritize winning point more than defend point
		*/
		for(int i = 0; i < 9; i++)
		{
			if(b->getPos(i) == 7)
			{
				b->place(2,i);
				return i+1;
			}
		}
		//capture center
		if((b->getPos(4) == 0))
		{
			b->place(2,4);
			return 5;
		}
		//cont: if center is captured by bot, aim for non-corner
		if((b->getPos(4) == 2) && !(b->getPos(1) == 2 || b->getPos(3) == 2 || b->getPos(5) == 2 || b->getPos(7) == 2))
		{
			while(b->getPos(1) == 0 || b->getPos(3) == 0 || b->getPos(5) == 0 || b->getPos(7) == 0)
			{
				int i = rand() % 4;
				switch(i)
				{
					case 0: if(b->getPos(1) == 0) {b->place(2,1); return 2;} break;
					case 1: if(b->getPos(3) == 0) {b->place(2,3); return 4;} break;
					case 2: if(b->getPos(5) == 0) {b->place(2,5); return 6;} break;
					case 3: if(b->getPos(7) == 0) {b->place(2,7); return 8;} break;
				}
			}
		}
		//capture corners
		while(b->getPos(0) == 0 || b->getPos(2) == 0 || b->getPos(6) == 0 || b->getPos(8) == 0)
		{
			int i = rand() % 4;
			switch(i)
			{
				case 0: if(b->getPos(0) == 0) {b->place(2,0); return 1;}
				case 1: if(b->getPos(2) == 0) {b->place(2,2); return 3;}
				case 2: if(b->getPos(6) == 0) {b->place(2,6); return 7;}
				case 3: if(b->getPos(8) == 0) {b->place(2,8); return 9;}
			}
		}
		//capture ANY available spot
		int hold = 0;
		while(true)
		{
			if(b->getPos(hold) == 0) 
			{
				b->place(2,hold); 
				return hold+1;
			}
			hold++;
		}
	}
private:
	
};
