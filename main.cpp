#include<iostream>
#include<cstdlib>
#include<ctime>
#include "board.h"
#include "bot.h"

using namespace std;

int main()
{
	Board b;
	Bot a;
	int turn = 1;
	int inp = 1;
	while(inp == 1)
	{
		b.reset();
		cout << "Who goes first? (1=Player|2=Bot): ";
		cin >> turn;
		if(turn < 1 && turn > 2) {turn = 1;}
		while(b.getState() == 1)
		{
			b.display();
			cout << endl;
			if(turn == 1)
			{
				cout << "Your turn: ";
				cin >> inp;
				while(!b.place(1,inp - 1))
				{
					cout << "Wrong input / slot occupied: ";
					cin >> inp;		
				}
				cout << endl;
				turn = 2;
			}
			else if(turn == 2)
			{
				cout << "Bot goes: " << a.think(&b) << endl << endl;
				turn = 1;
			}
		}
		b.display();
		cout << "Play again? (0|1): ";
		cin >> inp;
	}
	return 0;
}	
