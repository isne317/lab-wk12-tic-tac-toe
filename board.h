/*
	Board states:
	0: Empty
	1: Player
	2: Bot
	7: Player's winning point
	8: Bot's winning point
	9: Both's winning point
*/

class Board
{
public:
	Board()
	{
		for(int i = 0; i < 9; i++)
		{
			board[i] = 0;
		}
		gameState = 1;
		available = 9;
	}
	
	reset()
	{
		for(int i = 0; i < 9; i++)
		{
			board[i] = 0;
		}
		gameState = 1;
		available = 9;
	}
	
	bool place(int player, int index)
	{
		if(board[index] != 1 && board[index] != 2)
		{
			if(board[index] == 7 && player == 1)
			{
				triggerWin(1);	
			}
			else if(board[index] == 8 && player == 2)
			{
				triggerWin(2);	
			}
			else if(board[index] == 9)
			{
				triggerWin(player);	
			}
			board[index] = player;
			available--;
			boardCheck();
			return true;
		}
		else
		{
			return false;	
		}
	}
	
	boardCheck()
	{
		int holder[3];
		/*
			Horizontal
		*/	
		for(int i = 0; i < 3; i++)
		{
			holder[0] = 0;
			holder[1] = 0;
			holder[2] = 0;
			for(int j = 0; j < 3; j++)
			{
				switch(board[(i*3) + j])
				{
					case 0: holder[2] = j; break;
					case 1: holder[0]++; break;
					case 2: holder[1]++; break;	
					case 7: holder[2] = j; break;
					case 8: holder[2] = j; break;
					case 9: holder[2] = j; break;
				}
			}
			if(holder[0] == 2)
			{
				if(board[(i*3) + holder[2]] == 0) { board[(i*3) + holder[2]] = 7; }
				else if(board[(i*3) + holder[2]] == 8){ board[(i*3) + holder[2]] = 9; }
			}
			else if(holder[1] == 2)
			{
				if(board[(i*3) + holder[2]] == 0) { board[(i*3) + holder[2]] = 8; }
				else if(board[(i*3) + holder[2]] == 7) { board[(i*3) + holder[2]] = 9; }
			}
		}
		/*
			Vertical
		*/
		for(int i = 0; i < 3; i++)
		{
			holder[0] = 0;
			holder[1] = 0;
			holder[2] = 0;
			for(int j = 0; j < 3; j++)
			{
				switch(board[i + (j*3)])
				{
					case 0: holder[2] = j; break;
					case 1: holder[0]++; break;
					case 2: holder[1]++; break;	
					case 7: holder[2] = j; break;
					case 8: holder[2] = j; break;
					case 9: holder[2] = j; break;
				}
			}
			if(holder[0] == 2)
			{
				if(board[i + (holder[2]*3)] == 0) { board[i + (holder[2]*3)] = 7; }
				else if(board[i + (holder[2]*3)] == 8) { board[i + (holder[2]*3)] = 9; }
			}
			else if(holder[1] == 2)
			{
				if(board[i + (holder[2]*3)] == 0) { board[i + (holder[2]*3)] = 8; }
				else if(board[i + (holder[2]*3)] == 7) { board[i + (holder[2]*3)] = 9; }
			}
		}
		/*
			Diag - LD
		*/
		holder[0] = 0;
		holder[1] = 0;
		holder[2] = 0;
		for(int j = 0; j < 3; j++)
		{
			switch(board[(j*4)])
			{
				case 0: holder[2] = j; break;
				case 1: holder[0]++; break;
				case 2: holder[1]++; break;	
				case 7: holder[2] = j; break;
				case 8: holder[2] = j; break;
				case 9: holder[2] = j; break;
			}
		}
		if(holder[0] == 2)
		{
			if(board[holder[2]*4] == 0) { board[holder[2]*4] = 7; }
			else if(board[holder[2]*4] == 8) { board[holder[2]*4] = 9; }
		}
		else if(holder[1] == 2)
		{
			if(board[holder[2]*4] == 0) { board[holder[2]*4] = 8; }
			else if(board[holder[2]*4] == 7) { board[holder[2]*4] = 9; }
		}
		/*
			Diag - RD
		*/
		holder[0] = 0;
		holder[1] = 0;
		holder[2] = 0;
		for(int j = 0; j < 3; j++)
		{
			switch(board[2 + (j*2)])
			{
				case 0: holder[2] = j; break;
				case 1: holder[0]++; break;
				case 2: holder[1]++; break;	
				case 7: holder[2] = j; break;
				case 8: holder[2] = j; break;
				case 9: holder[2] = j; break;
			}
		}
		if(holder[0] == 2)
		{
			if(board[2 + (holder[2]*2)] == 0) { board[2 + (holder[2]*2)] = 7; }
			else if(board[2 + (holder[2]*2)] == 8) { board[2 + (holder[2]*2)] = 9; }
		}
		else if(holder[1] == 2)
		{
			if(board[2 + (holder[2]*2)] == 0) { board[2 + (holder[2]*2)] = 8; }
			else if(board[2 + (holder[2]*2)] == 7) { board[2 + (holder[2]*2)] = 9; }
		}
		if(gameState == 1 && available == 0)
		{
			gameState = 0;	
		}
	}
	
	triggerWin(int winner)
	{
		gameState = 1 + winner;	
	}
	
	display()
	{
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				switch(board[(i*3) + j])
				{
					case 0: std::cout << "[ ]"; break;
					case 1: std::cout << "[X]"; break;
					case 2: std::cout << "[O]"; break;
					case 7: std::cout << "[ ]"; break;
					case 8: std::cout << "[ ]"; break;
					case 9: std::cout << "[ ]"; break;
				}
			}
			std::cout << std::endl;
		}
		
		if(gameState == 2)
		{
			std::cout << "Player wins!" << std::endl;	
		}
		else if(gameState == 3)
		{
			std::cout << "Bot wins!" << std::endl;	
		}
		else if(gameState == 0)
		{
			std::cout << "Draw!" << std::endl;	
		}
	}
	
	int* getBoard()
	{
		return board;	
	}
	
	int getPos(int index)
	{
		return board[index];	
	}
	
	int getState()
	{
		return gameState;	
	}
	
private:

	int board[9];
	int gameState;
	int available;
	
};
